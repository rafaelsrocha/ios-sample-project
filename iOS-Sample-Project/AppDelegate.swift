//
//  AppDelegate.swift
//  iOS-Sample-Project
//
//  Created by Rafael Rocha on 1/20/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var managedObjectStore: RKManagedObjectStore?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        print("\(NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask))")
        
        let apiUrl = NSURL(string: "https://api.instagram.com")
        let objectManager = RKObjectManager(baseURL: apiUrl)
        
        let managedObjectModel = NSManagedObjectModel.mergedModelFromBundles(nil)
        managedObjectStore = RKManagedObjectStore(managedObjectModel: managedObjectModel)
        objectManager.managedObjectStore = managedObjectStore
        managedObjectStore!.createPersistentStoreCoordinator()
        
        let storePath = RKApplicationDataDirectory().stringByAppendingString("iOS-Sample-Project.sqlite")
        let seedPath = NSBundle.mainBundle().pathForResource("RKSeedDatabase", ofType: "sqlite")
        
        do {
            try managedObjectStore!.addSQLitePersistentStoreAtPath(storePath, fromSeedDatabaseAtPath: seedPath, withConfiguration: nil, options: nil)
        } catch {
            fatalError("Failed to add persistent store")
        }
        
        managedObjectStore!.createManagedObjectContexts()
        
        managedObjectStore!.managedObjectCache = RKInMemoryManagedObjectCache(managedObjectContext: managedObjectStore!.persistentStoreManagedObjectContext)
        
        navbarSetup()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    private func navbarSetup() {
        window?.backgroundColor = navbarColor
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().barTintColor = navbarColor
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    lazy var navbarColor: UIColor = {
        return UIColor(red: 176.0/255.0, green: 132.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }()
}

