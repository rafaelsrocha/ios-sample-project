//
//  BaseTableViewCell.m
//  iOS-Sample-Project
//
//  Created by Rafael Rocha on 1/21/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
