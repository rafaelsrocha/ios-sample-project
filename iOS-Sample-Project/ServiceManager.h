//
//  ServiceManager.h
//  iOS-Sample-Project
//
//  Created by Rafael Rocha on 1/21/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ServiceManagerDelegate <NSObject>
@optional
- (void)finishedRequest;
- (void)failedRequest;
@end

@interface ServiceManager : NSObject
@property (assign, nonatomic) id <ServiceManagerDelegate> delegate;

- (void)getInstagramPosts;
- (NSArray *)getPostsInDatabase;
@end
