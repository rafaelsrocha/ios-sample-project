//
//  Media+CoreDataProperties.swift
//  iOS-Sample-Project
//
//  Created by Rafael Rocha on 1/21/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Media {

    @NSManaged var link: String?
    @NSManaged var remoteId: String?
    @NSManaged var createdAt: String?
    @NSManaged var likesCount: NSNumber?
    @NSManaged var commentsCount: NSNumber?
    @NSManaged var username: String?
    @NSManaged var thumbnail: Image?
    @NSManaged var standard: Image?
    @NSManaged var low: Image?

}
