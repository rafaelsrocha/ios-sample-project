//
//  Image+CoreDataProperties.swift
//  iOS-Sample-Project
//
//  Created by Rafael Rocha on 1/21/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Image {

    @NSManaged var width: NSNumber?
    @NSManaged var height: NSNumber?
    @NSManaged var url: String?

}
