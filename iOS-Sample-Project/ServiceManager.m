//
//  ServiceManager.m
//  iOS-Sample-Project
//
//  Created by Rafael Rocha on 1/21/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import "ServiceManager.h"
#import "iOS_Sample_Project-Swift.h"
#import <RestKit/RestKit.h>

@interface ServiceManager ()
@property (strong, nonatomic) AppDelegate *appDelegate;
@end

@implementation ServiceManager

- (void)getInstagramPosts {
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    
    RKEntityMapping *mediaMapping = [RKEntityMapping mappingForEntityForName:@"Media"
                                                        inManagedObjectStore:self.appDelegate.managedObjectStore];
    mediaMapping.identificationAttributes = @[ @"remoteId" ];
    
    RKEntityMapping *imageMapping = [RKEntityMapping mappingForEntityForName:@"Image"
                                                         inManagedObjectStore:self.appDelegate.managedObjectStore];
    imageMapping.identificationAttributes = @[ @"url" ];
    
    [mediaMapping addAttributeMappingsFromDictionary:@{
                                                       @"id" : @"remoteId",
                                                       @"link" : @"link",
                                                       @"created_time" : @"createdAt",
                                                       @"likes.count" : @"likesCount",
                                                       @"comments.count" : @"commentsCount",
                                                       @"user.username" : @"username"
                                                       }];
    
    [imageMapping addAttributeMappingsFromDictionary:@{
                                                       @"url" : @"url",
                                                       @"width" : @"width",
                                                       @"height" : @"height"
                                                       }];
    
    [mediaMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"images.standard_resolution"
                                                                                 toKeyPath:@"standard"
                                                                               withMapping:imageMapping]];
    [mediaMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"images.thumbnail"
                                                                                 toKeyPath:@"thumbnail"
                                                                               withMapping:imageMapping]];
    [mediaMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"images.low_resolution"
                                                                                 toKeyPath:@"low"
                                                                               withMapping:imageMapping]];
    
    RKResponseDescriptor *mediaResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mediaMapping
                                                                                                 method:RKRequestMethodGET
                                                                                            pathPattern:@"/v1/tags/:tag_name/media/recent"
                                                                                                keyPath:@"data"
                                                                                            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [objectManager addResponseDescriptor:mediaResponseDescriptor];
    
    NSString *requestPath = [NSString stringWithFormat:@"/v1/tags/%@/media/recent", @"look"]; // can be any tag
    NSString *clientKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"InstagramClientID"];
    
    [objectManager getObjectsAtPath:requestPath
                         parameters:@{@"client_id" : clientKey}
                            success: ^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        if ([self.delegate respondsToSelector:@selector(finishedRequest)])
            [self.delegate finishedRequest];
    } failure: ^(RKObjectRequestOperation *operation, NSError *error) {
        RKLogError(@"Load failed with error: %@", error);
        
        if ([self.delegate respondsToSelector:@selector(failedRequest)])
            [self.delegate failedRequest];
    }];
}

- (AppDelegate *)appDelegate {
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark - Core Data

- (NSArray *)getPostsInDatabase {
    NSManagedObjectContext *context = [RKManagedObjectStore defaultStore].mainQueueManagedObjectContext;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Media"];
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:NO];
    fetchRequest.sortDescriptors = @[descriptor];
    
    NSError *error = nil;
    return [context executeFetchRequest:fetchRequest error:&error];
}

@end
