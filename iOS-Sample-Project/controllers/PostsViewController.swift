//
//  PostsViewController.swift
//  iOS-Sample-Project
//
//  Created by Rafael Rocha on 1/20/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class PostsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, ServiceManagerDelegate {

    @IBOutlet var tableView: UITableView!
    
    private var data: NSArray?
    private var shouldAnimate = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Look Search"
        
        tableView.registerNib(UINib(nibName: "PostCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "PostCell")
        tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.addSubview(refreshControl)
        
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: animationSwitch)
        animationSwitch.onTintColor = UIColor(red: 233.0/255.0, green: 233.0/255.0, blue: 233.0/255.0, alpha: 1.0)
        
        
        data = ServiceManager().getPostsInDatabase()
        tableView.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        defaultNavBar()
    }
    
    override func viewWillDisappear(animated: Bool) {
        navigationController?.navigationBar.layer.removeAllAnimations()
    }
    
    func switchTableAnimation() {
        shouldAnimate = animationSwitch.on
        SVProgressHUD.showSuccessWithStatus(animationSwitch.on ? "Cell Animation 👍" : "Cell Animation 👎", maskType: .Gradient)
    }
    
    func refresh() {
        let sm = ServiceManager()
        sm.delegate = self
        sm.getInstagramPosts()
    }
    
    func finishedRequest() {
        refreshControl.endRefreshing()
        data = ServiceManager().getPostsInDatabase()
        tableView.reloadData()
    }
    
    func failedRequest() {
        refreshControl.endRefreshing()
    }
    
    // MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data == nil || data?.count == 0 {
            tableView.backgroundView = noDataLabel
            return 0
        }
        
        tableView.backgroundView = nil
        return data!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("PostCell") as! PostCell
        
        let media = data![indexPath.row] as! Media
        
        cell.thumbnail.sd_setImageWithURL(NSURL(string: media.thumbnail!.url!), placeholderImage: UIImage())
        cell.usernameLabel.text = media.username!
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if !shouldAnimate { return }
        
        var rotation = CATransform3D()
        let angle = CGFloat((90.0 * M_PI) / 180.0)
        rotation = CATransform3DMakeRotation(angle, 0.0, 0.7, 0.4)
        rotation.m34 = 1.0 / -600
        
        cell.layer.shadowColor = UIColor.blackColor().CGColor
        cell.layer.shadowOffset = CGSizeMake(10, 10)
        cell.alpha = 0.0
        cell.layer.transform = rotation
        cell.layer.anchorPoint = CGPointMake(0, 0.5)
        
        // fix weird x position for invisible cells
        if cell.layer.position.x > 15 {
            cell.layer.position = CGPointMake(0, cell.layer.position.y)
        }
        
        UIView.beginAnimations("rotation", context: nil)
        UIView.setAnimationDuration(0.8)
        cell.layer.transform = CATransform3DIdentity
        cell.alpha = 1.0
        cell.layer.shadowOffset = CGSizeMake(0, 0)
        UIView.commitAnimations()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let post = data![indexPath.row]
        performSegueWithIdentifier("ShowPost", sender: post)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowPost" {
            let vc = segue.destinationViewController as! PostViewController
            vc.media = sender as! Media
        }
    }
    
    // MARK: - Initializers
    
    private lazy var noDataLabel: UILabel = {
        let label: UILabel = UILabel(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
        label.text = "There is no look available.\nPush to get new ones."
        label.textColor = UIColor.blackColor()
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.Center
        label.font = UIFont.systemFontOfSize(16.0)
        label.sizeToFit()
        return label
    }()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.blackColor()
        refreshControl.addTarget(self, action: "refresh", forControlEvents: .ValueChanged)
        
        return refreshControl
    }()
    
    lazy var animationSwitch: UISwitch = {
        let animSwitch = UISwitch()
        animSwitch.tintColor = UIColor(red: 176.0/255.0, green: 88.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        animSwitch.addTarget(self, action: "switchTableAnimation", forControlEvents: .ValueChanged)
        
        return animSwitch
    }()
}
