//
//  PostViewController.swift
//  iOS-Sample-Project
//
//  Created by Rafael Rocha on 1/21/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class PostViewController: BaseViewController {
    
    var media: Media!
    @IBOutlet var mediaImage: UIImageView!
    @IBOutlet var postedLabel: UILabel!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var likesLabel: UILabel!
    @IBOutlet var commentsLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        mediaImage.sd_setImageWithURL(NSURL(string: media.standard!.url!), placeholderImage: UIImage())
        
        let interval = NSDate(timeIntervalSince1970: (media.createdAt! as NSString).doubleValue)
        postedLabel.text = "Posted \(interval.timeAgo()) by"
        usernameLabel.text = media.username!
        likesLabel.text = "\(media.likesCount!) likes"
        commentsLabel.text = "\(media.commentsCount!) commennts"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Go to Post", style: UIBarButtonItemStyle.Plain, target: self, action: "openLink:")
    }
    
    override func viewWillAppear(animated: Bool) {
        gradientNavBar()
    }
    
    @IBAction func openLink(sender: AnyObject) {
        if  UIApplication.sharedApplication().canOpenURL(NSURL(string: media.link!)!) {
            UIApplication.sharedApplication().openURL(NSURL(string: media.link!)!)
        } else {
            SVProgressHUD.showErrorWithStatus("Could not open url 😕", maskType: .Gradient)
        }
    }
}
