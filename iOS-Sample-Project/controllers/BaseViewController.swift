//
//  BaseViewController.swift
//  iOS-Sample-Project
//
//  Created by Rafael Rocha on 1/20/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    func defaultNavBar() {
        clearNavBar()
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        navigationController?.navigationBar.barTintColor = appDelegate.navbarColor
        navigationController?.navigationBar.translucent = false
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }
    
    func gradientNavBar() {
        clearNavBar()
        
        let gradientView = UIView(frame: CGRectMake(0, 0, view.bounds.size.width, 64))
        
        let gradient = CAGradientLayer()
        gradient.frame = gradientView.bounds
        gradient.colors = [UIColor(white: 0.0, alpha: 0.8).CGColor, UIColor.clearColor().CGColor]
        
        gradientView.layer.insertSublayer(gradient, atIndex: 0)
        
        view.addSubview(gradientView)
    }
    
    func clearNavBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.translucent = true
        navigationController?.navigationBar.backgroundColor = UIColor .clearColor()
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    }
    
    lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }()
}
